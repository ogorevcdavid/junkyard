#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.

;MEMORY COPY/PASTE capslock+F<x> for copy and  capslock+shift+F<x> for paste
MemoryCopy(ByRef Valu)
{
    OldClip := ClipBoardAll
    Send ^c
    Sleep 50
    Valu := ClipBoardAll
    ClipBoard := OldClip

}

MemoryPaste(ByRef Valu)
{
    OldClip := ClipBoardAll
    ClipBoard := Valu  
    Send ^v        
    Sleep 50
    ClipBoard := OldClip
}

;Disable win+space keyboard layout switch
#space::
Return

;Puase/Break send win+space to change keyboard layout switch
Pause::
    Send, {LWin down}{space}{LWin up}
Return

global Clip1 := ""
global Clip2 := ""
global Clip3 := ""
global Clip4 := ""


; Print current date
!c::
    SendInput !l
    Sleep 50


    line := clipboard

    SendInput ^c
    Sleep 50



    ClipBoard := line " - " clipboard

Return



capslock::return

#If GetKeyState("CapsLock", "P")

F1::MemoryCopy(Clip1)
+F1::MemoryPaste(Clip1)

F2::MemoryCopy(Clip2)
+F2::MemoryPaste(Clip2)

F3::MemoryCopy(Clip3)
+F3::MemoryPaste(Clip3)

F4::MemoryCopy(Clip4)
+F4::MemoryPaste(Clip4)

;sumniki mali
s::š
z::ž
c::č

;sumniki veliki
+s::Š
+z::Ž
+c::Č

i:: Send, {Up}
k:: Send, {Down}
j:: Send, {Left}
l:: Send, {Right}

+i:: Send, +{Up}
+k:: Send, +{Down}
+j:: Send, +{Left}
+l:: Send, +{Right}


u:: Send, {Home}
o:: Send, {End}
+u:: Send, +{Home}
+o:: Send, +{End}

y:: Send, {PgUp}
h:: Send, {PgDn}
+y:: Send, +{PgUp}
+h:: Send, +{PgDn}

;Media buttons
PgUp:: Send, {Volume_Up 1}
PgDn:: Send, {Volume_Down 1}
Home:: Send, {Media_Prev}
End:: Send, {Media_Next}
Pause:: Send, {Media_Play_Pause}

BACKSPACE:: Send, {Delete}


; Print current date
d::
    FormatTime, CurrentDateTime,, yyyy-MM-dd
    SendInput %CurrentDateTime%
Return
; Print current date
t::
    FormatTime, CurrentDateTime,, yyyy-MM-dd HH:mm:ss
    SendInput %CurrentDateTime%
Return

; Clean paste, remove all formatting
v::
    ClipClean = %ClipBoardAll%
    ClipBoard = %ClipBoard%       ; Convert to text
    Send ^v                       ; For best compatibility: SendPlay
    Sleep 50                     ; Don't change clipboard while it is pasted! (Sleep > 0)
    ClipBoard = %ClipClean%           ; Restore original ClipBoard
    VarSetCapacity(ClipClean, 0)      ; Free memory
Return

; Paste without clipboard
+v::
    ClipBucket5 = {Raw}%ClipBoard% 
    ;sendinput % ClipBucket5
    send % RegExReplace(ClipBucket5, "\r\n?|\n\r?", "`n")

Return


#If

!#c::
    Run C:\Users\ogd\AppData\Local\Programs\Microsoft VS Code\Code.exe
Return

!#f::^^
    Run explorer
Return



!#d::
    Run C:\Users\ogd\AppData\Local\Programs\Azure Data Studio\azuredatastudio.exe
Return

!#x::
    Run powershell
Return

!#z::
    Run powershell Start-Process explorer -Verb runAs
Return

!#i::
    Run firefox
Return




;mirror






; space & Tab::
;     if GetKeyState("Scrolllock", "T") 
;         send {Enter}
; return

; space & `::
;     if GetKeyState("Scrolllock", "T") 
;         send {BACKSPACE}
; return


; #UseHook, On

         

#If GetKeyState("ScrollLock", "T")


; If spacebar didn't modify anything, send a real space keystroke upon release.
space::
Send {space}
return

sendMirrorKey(ky){
    if GetKeyState("Scrolllock", "T") 
        send %ky%
}

sendMirrorKeyShift(ky,sky){
    if GetKeyState("Alt", "D") {
        send %sky%
    }
    else{
        send %ky%

    }
}


4:: send 1
5:: send 2
6:: send 3
7:: send 4
8:: send 5

space & 1:: send {BackSpace}
space & 2:: sendMirrorKeyShift("=","+")
space & 3:: sendMirrorKeyShift("-","_")
space & 4:: send 0
space & 5:: sendMirrorKeyShift("9","*")
space & 6:: sendMirrorKeyShift("8","*")
space & 7:: sendMirrorKeyShift("7","*")
space & 8:: send 6

r:: send q
t:: send w
y:: send e
u:: send r
i:: send t

space & w:: send {]}
space & e:: send {[}
space & r:: send p
space & t:: send o
space & y:: send i
space & u:: send u
space & i:: send y

f:: send a
g:: send s
h:: send d
j:: send f
k:: send g

space & a:: send {Enter}
space & s:: send {\}

space & d:: send {'}
space & f:: send {;}
space & g:: send l
space & h:: send k
space & j:: send j
space & k:: send h

v:: send z
b:: send x
n:: send c
m:: send v
,:: send b

space & v:: send {/}
space & b:: send {.}
space & n:: send {,}
space & m:: send m
space & ,:: send n

#If 
